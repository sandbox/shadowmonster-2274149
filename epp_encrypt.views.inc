<?php

/**
 * Implements hook_views_data().
 */
function epp_encrypt_views_data() {
    
    $instances = field_info_instances();
    $fields_info = field_info_fields();
    
    // Get only epp encrypt fields.
    foreach($instances as $si => $base_in) {
        foreach($base_in as $object => $obv) {
            foreach($obv as $field => $fdv) {
                if($fdv['widget']['module'] == 'epp_encrypt') {
                    $table_load = $fields_info[$field]['storage']['details']['sql']['FIELD_LOAD_CURRENT'];
                    foreach($table_load as $ti => $tv) {
                        $table = $ti;
                        $table_value = $tv['value'];
                        $table_format = $tv['format'];
                        $data[$table]['table']['group'] = t('Encrypted Fields');
                        /*$data[$table]['table']['base']= array(
                            'field' => 'entity_id',
                            'title' => $table,
                            //'help' => t('Example table contains example content and can be related to nodes.'),
                            'weight' => -10,
                            );*/
                        $main_table = $si;
                        if($si == 'node') {
                            $join_field = 'nid';
                            }
                        elseif($si == 'profile2') {
                            $main_table = 'profile';
                            $join_field = 'pid';
                            }
                        else {
                            $join_field = 'entity_id';
                            }
                       

                            $data[$table]['table']['join'][$main_table] = array(
                                'type' => 'LEFT',
                                'left_field' => $join_field,
                                'field' => 'entity_id',
                                );
                            $data[$table]['nid'] = array(
                            'title' => t('Example content'),
                            'help' => t('Some example content that references a node.'),
                            'relationship' => array(
                                'base' => 'node', // The name of the table to join with.
                                'base field' => 'nid', // The name of the field on the joined table.
                                // 'field' => 'nid' -- see hook_views_data_alter(); not needed here.
                                'handler' => 'views_handler_relationship',
                                'label' => t('Default label for the relationship'),
                                'title' => t('Title shown when adding the relationship'),
                                'help' => t('More information on this relationship'),
                                ),
                            );

                        
                         $data[$table][$table_value] = array(
                            'title' => $fdv['label'],
                            'help' => t("Field: @field", array("@field" => $table)),
                            'field' => array(
                                'handler' => 'views_handler_epp_encrypt_field',
                                'click sortable' => TRUE, // This is use by the table display plugin.
                                ),
                            /*'sort' => array(
                                'handler' => 'views_handler_sort',
                                ),*/
                            'filter' => array(
                                'handler' => 'views_handler_epp_encrypt_filter_string',
                                ),
                            /*'argument' => array(
                                'handler' => 'views_handler_epp_encrypt_argument',
                                ),*/
                            );
                        }
                    }
                }
            }
        }
    
    /*print_r($instances);
    print_r($fields_info);
    die();*/
    
    //$data['epp_encrypt']['table']['group'] = t('EPP Encrypt');
    $data['epp_encrypt']['table']['join'] = array(
        // Exist in all views.
        '#global' => array(),
        );
 
    /*$data['epp_encrypt']['epp_encrypt'] = array(
        'title' => t('My custom field'),
        'help' => t('My custom field displays the word "Some custom markup".'),
        'field' => array(
        'handler' => 'views_handler_epp_encrypt_field',
        ),
        );*/
 
  return $data;
}

//hook_views_da