<?php

/**
 * @file
 * Custom views handler definition.
 */
 
/**
 * Custom handler class.
 *
 * @ingroup views_field_handlers
 */
class views_handler_epp_encrypt_field extends views_handler_field {
  /**
   * {@inheritdoc}
   *
   * Perform any database or cache data retrieval here. In this example there is
   * none.
   */
  function query() {
 $this->ensure_my_table();
    // Add the field.
    $params = $this->options['group_type'] != 'group' ? array('function' => $this->options['group_type']) : array();
    //print_r($this);
    //die();
    
    // Gen encryption key.
    $config = encrypt_get_default_config();
    $provider_settings = $config['provider_settings'];
    $key_provider = $config['provider'];
    $encrypt_key = encrypt_get_key_from_key_provider($key_provider, $provider_settings);
    
    if($this->definition['handler'] == 'views_handler_epp_encrypt_field') {
        $this->field_alias = $this->query->add_field(NULL, "DECRYPT(DECODE(" . $this->table_alias . "." . $this->real_field . ", 'base64'), '" . $encrypt_key . "', 'AES')", $this->table_alias, $params);
        }
    else {
        $this->field_alias = $this->query->add_field($this->table_alias, $this->real_field, NULL, $params);
        }

    $this->add_additional_fields();

  }
 function add_additional_fields($fields = NULL) {
 if (!isset($fields)) {
      // notice check
      if (empty($this->additional_fields)) {
        return;
      }
      $fields = $this->additional_fields;
    }

    $group_params = array();
    if ($this->options['group_type'] != 'group') {
      $group_params = array(
        'function' => $this->options['group_type'],
      );
    }

    if (!empty($fields) && is_array($fields)) {
      foreach ($fields as $identifier => $info) {
        if (is_array($info)) {
          if (isset($info['table'])) {
            $table_alias = $this->query->ensure_table($info['table'], $this->relationship);
          }
          else {
            $table_alias = $this->table_alias;
          }

          if (empty($table_alias)) {
            debug(t('Handler @handler tried to add additional_field @identifier but @table could not be added!', array('@handler' => $this->definition['handler'], '@identifier' => $identifier, '@table' => $info['table'])));
            $this->aliases[$identifier] = 'broken';
            continue;
          }

          $params = array();
          if (!empty($info['params'])) {
            $params = $info['params'];
          }

          $params += $group_params;
          $this->aliases[$identifier] = $this->query->add_field($table_alias, $info['field'], NULL, $params);
        
          
        }
        else {
            
          $this->aliases[$info] = $this->query->add_field($this->table_alias, $info, NULL, $group_params);
        }
      }
    }
    

 }
  /**
   * {@inheritdoc}
   *
   * Modify any end user views settings here. Debug $options to view the field
   * settings you can change.
   */
  function option_definition() {
    $options = parent::option_definition();
    return $options;
  }
 
  /**
   * {@inheritdoc}
   *
   * Make changes to the field settings form seen by the end user when adding
   * your field.
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
  }
 
  /**
   * Render callback handler.
   *
   * Return the markup that will appear in the rendered field.
   */
  function render($values) {

      $alias = $this->field_alias;
    $value = $values->{$alias};
    
    /*
     * This is needed to see preview in Views UI otherway will return php json
     * encode error and will not diplay preview.
     */
    if(!mb_check_encoding($value, 'UTF-8')) {
        $value = utf8_encode($value);
        }
      
    return $value;
  }
}

