<?php

class views_handler_epp_encrypt_filter_string extends views_handler_filter_string {
    
    protected $php_static_variable = NULL;

function operators() {
    /*
     * DEV: Before we allow any of operators we have to test it first.
     */
    
    $operators = array(
      '=' => array(
        'title' => t('Is equal to'),
        'short' => t('='),
        'method' => 'op_equal',
        'values' => 1,
      ),
      '!=' => array(
        'title' => t('Is not equal to'),
        'short' => t('!='),
        'method' => 'op_equal',
        'values' => 1,
      ),
      'contains' => array(
        'title' => t('Contains'),
        'short' => t('contains'),
        'method' => 'op_contains',
        'values' => 1,
      ),
     /* 'word' => array(
        'title' => t('Contains any word'),
        'short' => t('has word'),
        'method' => 'op_word',
        'values' => 1,
      ),
      'allwords' => array(
        'title' => t('Contains all words'),
        'short' => t('has all'),
        'method' => 'op_word',
        'values' => 1,
      ),
      'starts' => array(
        'title' => t('Starts with'),
        'short' => t('begins'),
        'method' => 'op_starts',
        'values' => 1,
      ),
      'not_starts' => array(
        'title' => t('Does not start with'),
        'short' => t('not_begins'),
        'method' => 'op_not_starts',
        'values' => 1,
      ),
      'ends' => array(
        'title' => t('Ends with'),
        'short' => t('ends'),
        'method' => 'op_ends',
        'values' => 1,
      ),
      'not_ends' => array(
        'title' => t('Does not end with'),
        'short' => t('not_ends'),
        'method' => 'op_not_ends',
        'values' => 1,
      ),*/
      'not' => array(
        'title' => t('Does not contain'),
        'short' => t('!has'),
        'method' => 'op_not',
        'values' => 1,
      ),
      /*'shorterthan' => array(
        'title' => t('Length is shorter than'),
        'short' => t('shorter than'),
        'method' => 'op_shorter',
        'values' => 1,
      ),
      'longerthan' => array(
        'title' => t('Length is longer than'),
        'short' => t('longer than'),
        'method' => 'op_longer',
        'values' => 1,
      ),*/
    );
    // if the definition allows for the empty operator, add it.
    if (!empty($this->definition['allow empty'])) {
      $operators += array(
        'empty' => array(
          'title' => t('Is empty (NULL)'),
          'method' => 'op_empty',
          'short' => t('empty'),
          'values' => 0,
        ),
        'not empty' => array(
          'title' => t('Is not empty (NOT NULL)'),
          'method' => 'op_empty',
          'short' => t('not empty'),
          'values' => 0,
        ),
      );
    }
    // We do not supper MySQL yet.
    /*if (Database::getConnection()->databaseType() == 'mysql') {
      $operators += array(
        'regular_expression' => array(
          'title' => t('Regular expression'),
          'short' => t('regex'),
          'method' => 'op_regex',
          'values' => 1,
        ),
      );
    }*/

    return $operators;
  }
  
  function query() {
    $this->view->epp_encrypt = TRUE;
  }
  
    function op_equal($field) {
        $this->view->epp_encrypt = TRUE;
        /*
         * TODO: try implement direct query - this below didn't work :(
        */
        /*
        $config = encrypt_get_default_config();
        $provider_settings = $config['provider_settings'];
        $key_provider = $config['provider'];
        $encrypt_key = encrypt_get_key_from_key_provider($key_provider, $provider_settings);
        
        $this->query->add_where_expression($this->options['group'], $field . " = ENCODE(ENCRYPT('" . $this->value . "', :db_encrypt_key, 'AES'), 'base64')", array(':db_encrypt_key' => $encrypt_key));
        */
    
    /*if($this->definition['handler'] == 'views_handler_epp_encrypt_field') {
        $this->field_alias = $this->query->add_field(NULL, "DECRYPT(DECODE(" . $this->table_alias . "." . $this->real_field . ", 'base64'), '" . $encrypt_key . "', 'AES')", $this->table_alias, $params);
        }*/
        
        //$this->query->add_where($this->options['group'], $field, $this->value, $this->operator());
        //$this->query->add_where_expression($this->options['group'], $field . " = ENCODE(ENCRYPT('" . $this->value . "', :db_encrypt_key, 'AES'), 'base64')", array(':db_encrypt_key' => $encrypt_key));
        }

    function op_contains($field) {
        /*
         * We can't do it on DB level so we will put it in PHP as row exclusion.
         */
        }
        
    /**
   *
   * @see views_php_views_pre_execute()
   */
  function php_pre_execute() {
    // Ecexute static PHP code.
    if (!empty($this->options['php_setup'])) {
      $function = create_function('$view, $handler, &$static', $this->options['php_setup'] . ';');
      ob_start();
      $function($this->view, $this, $this->php_static_variable);
      ob_end_clean();
    }
  }

  /**
   *
   * @see views_php_views_post_execute()
   */
  function php_post_execute() {

      
      /*print_r($this);
      die();*/
      
    //if($operator == '=') {
        //ob_start();
        foreach ($this->view->result as $i => $row) {
            foreach ($this->view->display_handler->get_handlers('field') as $field => $handler) {
                if($field == $this->field) {
                    if($this->operator == '=') {
                        // TODO: add case sensitive as option.
                        if(trim(strtolower($row->{$handler->field_alias})) != trim(strtolower($this->value))) {
                            unset($this->view->result[$i]);
                            }
                        }
                    if($this->operator == '!=') {
                        // TODO: add case sensitive as option.
                        if(trim(strtolower($row->{$handler->field_alias})) == trim(strtolower($this->value))) {
                            unset($this->view->result[$i]);
                            }
                        }
                    if($this->operator == 'contains' and !empty($this->value)) {
                        // TODO: add case sensitive as option.
                        if(!strstr(trim(strtolower($row->{$handler->field_alias})), trim(strtolower($this->value)))) {
                            unset($this->view->result[$i]);
                            }
                        }
                    if($this->operator == 'not') {
                        // TODO: add case sensitive as option.
                        if(strstr(trim(strtolower($row->{$handler->field_alias})), trim(strtolower($this->value)))) {
                            unset($this->view->result[$i]);
                            }
                        }
                    if($this->operator == 'empty') {

                        if(!empty($row->{$handler->field_alias})) {
                            unset($this->view->result[$i]);
                            }
                        }
                    if($this->operator == 'not empty') {
                        //$this_value = $this->value;
                        // TODO: add case sensitive as option.
                        if(empty($row->{$handler->field_alias})) {
                            unset($this->view->result[$i]);
                            }
                        }
                    }
                }
            }
            //ob_end_clean();
        //}   
        //die();
    // Evaluate the PHP code.
    /*if (!empty($this->options['php_filter'])) {
      $function = create_function('$view, $handler, &$static, $row, $data', $this->options['php_filter'] . ';');
      ob_start();
      foreach ($this->view->result as $i => $row) {
        $normalized_row = new stdClass;
        foreach ($this->view->display_handler->get_handlers('field') as $field => $handler) {
          $normalized_row->$field = isset($row->{$handler->field_alias}) ? $row->{$handler->field_alias} : NULL;
        }
        if ($function($this->view, $this, $this->php_static_variable, $normalized_row, $row)) {
          unset($this->view->result[$i]);
        }
      }
      ob_end_clean();
    }*/
  }
    }

