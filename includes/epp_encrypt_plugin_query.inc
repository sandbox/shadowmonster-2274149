<?php

/**
 * A (fake) pager plugin that wraps around the actual query.
 * Original code form views_php module.
 *
 * @ingroup views_query_plugins
 */
class epp_encrypt_plugin_query extends epp_encrypt_plugin_wrapper {

  /**
   * Implements views_plugin_query#execute().
   */
  function execute(&$view) {
    $pager = new epp_encrypt_plugin_pager();
    $pager->php_wrap($this->wrapped->pager);

    $this->wrapped->execute($view);

    $pager->php_unwrap();
  }
}

